talk.pdf: talk.org
	mkdir _build
	emacs -batch $< --eval='(setq org-src-preserve-indentation t)' \
	                --eval='(org-beamer-export-to-pdf)'
clean:
	rm -rf _build
